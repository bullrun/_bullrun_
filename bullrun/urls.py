"""bullrun URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    path('', views.index),
    path('news', views.news),
    path('stat', views.stat),
    path('create', views.create),
    path('account', views.account),
    path('delete/<int:id>/', views.delete),
    path('edit/<int:id>/', views.edit),
    path('login', views.MyProjectLoginView.as_view()),
    path('register', views.RegisterUserView.as_view()),
    path('logout', views.MyProjectLogout.as_view()),
    path('admin/', admin.site.urls),
    path('reset_password/', auth_views.PasswordResetView.as_view(template_name="bullrun/password_reset.html"), name="reset_password"),
    path('reset_password_sent/', auth_views.PasswordResetDoneView.as_view(template_name="bullrun/password_reset_sent.html"), name="password_reset_done"),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name="bullrun/password_reset_form.html"), name="password_reset_confirm"),
    path('reset_password_complete/', auth_views.PasswordResetCompleteView.as_view(template_name="bullrun/password_reset_done.html"), name="password_reset_complete"),
    path('dollar', views.dollar),
    path('bitcoin', views.bitcoin),
    path('stocks', views.stocks)

]
