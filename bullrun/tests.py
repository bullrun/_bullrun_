from django.test import TestCase
from .models import Stats
from django.contrib.auth.models import User


class StatsTestCase(TestCase):
    def setUp(self):

        Stats.objects.create(title="aaa", reason="bbb", result="ccc")

    def test(self):
        stat1 = Stats.objects.get(title="aaa")
        self.assertEqual(stat1.reason, "bbb")


class UsersLoginTest(TestCase):
    def setUp(self):
        test_user1 = User.objects.create_user(username='test_user1', email='test1@mail.ru', password='Im_test_user')
        test_user2 = User.objects.create_user(username='test_user2', email='test2@mail.ru', password='123456')
        test_admin = User.objects.create_user(username='TestAdmin', email='testadmin@mail.ru', password='Im_admin', is_superuser=1, is_staff=1)
        test_not_admin = User.objects.create_user(username='NotAdmin', email='notadmin@mail.ru', password='Im_not_admin', is_superuser=0, is_staff=0)

    def test_login_users(self):
        login1 = (self.client.login(username='test_user1', password='Im_test_user'))
        self.assertTrue(login1)
        login2 = (self.client.login(username='test_user2', password='654321'))
        self.assertFalse(login2)

    def test_login_admin(self):
        self.client.login(username='TestAdmin', password='Im_admin')
        response = self.client.get('/admin/')
        self.assertEqual(response.status_code, 200)

    def test_login_not_admin(self):
        self.client.login(username='NotAdmin', password='Im_not_admin')
        response = self.client.get('/login')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/admin/')
        self.assertEqual(response.status_code, 302)


class PasswordChangeCheck(TestCase):
    def setUp(self):
        test_user = User.objects.create_user(username='test_user', email='testu@mail.ru', password='old_pass_user')
        test_admin = User.objects.create_user(username='test_admin', email='testa@mail.ru', password='old_pass_admin', is_superuser=1, is_staff=1)

    def test_new_password_user(self):
        tu = User.objects.get(username='test_user')
        tu.set_password('new_pass_user')
        tu.save()
        self.assertFalse(self.client.login(username='test_user', password='old_pass_user'))
        self.assertTrue(self.client.login(username='test_user', password='new_pass_user'))

    def test_new_password_admin(self):
        ta = User.objects.get(username='test_admin')
        ta.set_password('new_pass_admin')
        ta.save()
        self.client.login(username='test_admin', password='old_pass_admin')
        response = self.client.get('/admin/')
        self.assertEqual(response.status_code, 302)
        self.client.login(username='test_admin', password='new_pass_admin')
        response = self.client.get('/admin/')
        self.assertEqual(response.status_code, 200)
