from django.db import models


class Stats (models.Model):
    title = models.CharField('Действие', max_length=50)
    reason = models.TextField('Причина')
    result = models.CharField('Результат', max_length=50)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Действие'
        verbose_name_plural = 'Действия'