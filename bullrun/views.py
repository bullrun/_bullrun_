from django.shortcuts import render, redirect
from .models import Stats
from .forms import StatsForm, AuthUserForm, RegisterUserForm
from django.http import HttpResponseRedirect
from django.http import HttpResponseNotFound
from django.contrib.auth.views import LoginView, LogoutView
from django.views.generic import CreateView
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from newsapi import NewsApiClient
from bs4 import BeautifulSoup
import requests

def index(request):
    return render(request, 'bullrun/index.html')


def account(request):
    return render(request, 'bullrun/account.html')


def news(request):

    newsapi = NewsApiClient(api_key='fa3523bba89244239572caba35bb4c91')

    top = newsapi.get_everything( sort_by='relevancy', q='bitcoin')

    l = top['articles']

    desc = []

    news = []

    img = []


    for i in range(len(l)):
        f = l[i]

        news.append(f['title'])

        desc.append(f['description'])

        img.append(f['urlToImage'])


    mylist = zip(news, desc, img)

    return render(request, 'bullrun/news.html', context={"mylist": mylist})


def stat(request):
    stats = Stats.objects.order_by('id')
    return render(request, 'bullrun/stat.html', {'stats': stats})


def create(request):
    error = ''
    if request.method == 'POST':
        form = StatsForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/stat')
        else:
            error = 'Форма была неверной'
    form = StatsForm()
    context = {
        'form': form,
        'error': error
    }
    return render(request, 'bullrun/create.html', context)


def edit(request, id):
    try:
        stats = Stats.objects.get(id=id)

        if request.method == "POST":
            stats.title = request.POST.get("title")
            stats.reason = request.POST.get("reason")
            stats.result = request.POST.get("result")
            stats.save()
            return HttpResponseRedirect("/stat")
        else:
            return render(request, "bullrun/edit.html", {"stats": stats})
    except Stats.DoesNotExist:
        return HttpResponseNotFound("<h2>Note not found</h2>")


def delete(request, id):
    try:
        stat = Stats.objects.get(id=id)
        stat.delete()
        return HttpResponseRedirect("/stat")
    except Stats.DoesNotExist:
        return HttpResponseNotFound("<h2>Note not found</h2>")


class MyProjectLoginView(LoginView):
    template_name = 'bullrun/login.html'
    form_class = AuthUserForm
    success_url = '/'

    def get_success_url(self):
        return self.success_url


class RegisterUserView(CreateView):
    model = User
    template_name = 'bullrun/register.html'
    form_class = RegisterUserForm
    success_url = '/'
    success_msg = 'Пользователь успешно создан'

    def form_valid(self, form):
        form_valid = super(RegisterUserView, self).form_valid(form)
        username = form.cleaned_data["username"]
        password = form.cleaned_data["password"]
        aut_user = authenticate(username=username, password=password)
        login(self.request, aut_user)
        return form_valid


class MyProjectLogout(LogoutView):
    next_page = '/'


def dollar(request):
    DOLLAR_RUB = 'https://www.google.ru/search?newwindow=1&sxsrf=ALeKk01K0ui8ftBM5Q44b2mh4dJx99CgLg%3A1608022130014&ei=cnjYX-g0j5LA8A-Lw534Ag&q=курс+доллара&oq=ку&gs_lcp=CgZwc3ktYWIQAxgAMgQIIxAnMgQIIxAnMgQIIxAnMgQIABBDMgQIABBDMgUIABCxAzICCAAyBQgAELEDMgIIADIFCAAQsQM6BwgjEOoCECc6CAgAELEDEIMBOgsIABCxAxDHARCvAVCCuBdYs8EXYJXLF2gDcAF4AIABjwGIAbcDkgEDMS4zmAEAoAEBqgEHZ3dzLXdperABCsABAQ&sclient=psy-ab'
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 OPR/71.0.3770.456 (Edition Campaign 34)'}

    full_page_dollar = requests.get(DOLLAR_RUB, headers=headers)

    soup_dollar = BeautifulSoup(full_page_dollar.content, 'html.parser')

    convert_dollar = soup_dollar.findAll("span", {"class": "DFlfde", "class": "SwHCTb", "data-precision": 2})

    return render(request, 'bullrun/dollar.html', {'dollar': convert_dollar[0].text})


def bitcoin(request):
    BITCOIN_RUB = 'https://www.google.ru/search?newwindow=1&sxsrf=ALeKk013vtWFRmFdxcW2SwlnKkSM8ZpDdQ%3A1608564871482&source=hp&ei=h8DgX8SOGsyJwPAPgLSU6A8&q=курс+биткоина&oq=курс+биткоина&gs_lcp=CgZwc3ktYWIQAzIJCCMQJxBGEIICMggIABCxAxCDATICCAAyCAgAELEDEIMBMggIABCxAxCDATICCAAyAggAMgIIADICCAAyAggAOgcIIxDqAhAnOgUIABCxAzoECCMQJzoICAAQxwEQrwE6DAgjELECECcQRhCCAjoHCCMQsQIQJzoCCC46CggAELEDEIMBEApQ5OsBWJylAmDCpgJoA3AAeAGAAfwBiAGFDJIBBjEzLjAuMpgBAKABAaoBB2d3cy13aXqwAQo&sclient=psy-ab&ved=0ahUKEwiE7omOs9_tAhXMBBAIHQAaBf0Q4dUDCAY&uact=5'
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 OPR/71.0.3770.456 (Edition Campaign 34)'}

    full_page_bitcoin = requests.get(BITCOIN_RUB, headers=headers)

    soup_bitcoin = BeautifulSoup(full_page_bitcoin.content, 'html.parser')

    convert_bitcoin = soup_bitcoin.findAll("span", {"class": "DFlfde", "class": "SwHCTb", "data-precision": 2})

    return render(request, 'bullrun/bitcoin.html', {'bitcoin': convert_bitcoin[0].text})


def stocks(request):
    STOCKS_RUB = 'https://www.google.ru/search?newwindow=1&sxsrf=ALeKk01YCRY3sDx5khQp-7pKuiKcVGdNVw%3A1608564909692&ei=rcDgX-_aKY-yqwGJop_4Cg&q=курс+акций+тесла&oq=курс+ак&gs_lcp=CgZwc3ktYWIQAxgEMggIABCxAxCDATIICAAQsQMQgwEyCAgAELEDEIMBMggIABCxAxCDATIICAAQsQMQgwEyAggAMggIABCxAxCDATICCAAyAggAMgIIADoECAAQCjoECAAQDToECCMQJzoKCAAQsQMQgwEQQzoECAAQQ1D-kxxY3qscYNLBHGgBcAF4AIAB5g2IAaQekgEFNC44LTKYAQCgAQGqAQdnd3Mtd2l6wAEB&sclient=psy-ab'
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36 OPR/71.0.3770.456 (Edition Campaign 34)'}

    full_page_stocks = requests.get(STOCKS_RUB, headers=headers)

    soup_stocks = BeautifulSoup(full_page_stocks.content, 'html.parser')

    convert_stocks = soup_stocks.findAll("span", {"class": "IsqQVc", "class": "NprOob", "class": "XcVN5d"})

    return render(request, 'bullrun/stocks.html', {'stocks': convert_stocks[0].text})
