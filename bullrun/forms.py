from .models import Stats
from django.forms import ModelForm, TextInput, Textarea
from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User


class StatsForm(ModelForm):

    class Meta:
        model = Stats
        fields = ["title", "reason", "result"]
        widgets = {
            "title": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Введите название'
            }),
            "reason": Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Введите причину'
            }),
            "result": Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Введите причину'
            })

        }


class AuthUserForm(AuthenticationForm, forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'password')
        widgets = {
            "username": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Введите логин'
            }),
            "password": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Введите пароль'
            })

        }


class RegisterUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'password', 'email')

        widgets = {
            "username": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Придумайте логин'
            }),
            "password": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Придумайте пароль'
            }),
            "email": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Введите почту'
            })

        }

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
            return user